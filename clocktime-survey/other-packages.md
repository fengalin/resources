## gstreamer-app

### Manual

| scope  | function    | arg. | M | ret | comment |
|:------ |:----------- |:---- |:-:|:---:| ------- |
| AppSrc | set_latency | min  | O |     | Doc: None => default latency calculation |
|        |             | max  | O |     |         |
| AppSrc | latency     |      |   |  T  | ret: (min: O, max: O) |

> @slomo: ✅

### Auto

| scope   | function         | arg.    | M | ret | comment |
|:------- |:---------------- |:------- |:-:|:---:| ------- |
| AppSink | try_pull_preroll | timeout | O |     | Code: None => don't timeout |
| AppSink | try_pull_sample  | timeout | O |     | Code: None => don't timeout |

> @slomo: ✅

| scope  | function     | arg.     | M | ret | comment |
|:------ |:------------ |:-------- |:-:|:---:| ------- |
| AppSrc | duration     |          |   |  O  | Doc: None => unknown |
| AppSrc | set_duration | duration | O |     | Doc: None => unknown |

> @slomo: ✅

## gstreamer-audio

### Manual

| scope                 | function | arg. | M | ret | comment |
|:--------------------- |:-------- |:---- |:-:|:---:| ------- |
| AudioEncoderExtManual | latency  |      |   |  T  | ret: (min: M, max: O) |

> @slomo: min mandatory (0 default!), max optional. Needs check in C.

| scope            | function | arg.      | M | ret | comment |
|:---------------- |:-------- |:--------- |:-:|:---:| ------- |
| AudioStreamAlign | process  | timestamp | M |  T  | arg not checked in C, ret: (out_timestamp: M, out_duration: M) |

> @slomo: All mandatory (in and out), needs C checks

### Auto

| scope            | function                | arg.                | M | ret | comment |
|:---------------- |:----------------------- |:------------------- |:-:|:---:| ------- |
| AudioBaseSinkExt | alignment_threshold     |                     |   |  M  |  |
| AudioBaseSinkExt | discont_wait            |                     |   |  M  |  |
| AudioBaseSinkExt | set_alignment_threshold | alignment_threshold | M |     |  | x
| AudioBaseSinkExt | set_discont_wait        | discont_wait        | M |     |  | x

> @slomo: all mandatory, needs C checks

| scope           | function        | arg.      | M | ret | comment |
|:--------------- |:--------------- |:--------- |:-:|:---:| ------- |
| AudioDecoderExt | latency         |           |   |  T  | ret: (min: M, max: O) |
| AudioDecoderExt | min_latency     |           |   |  M  |  |
| AudioDecoderExt | tolerance       |           |   |  M  |  |
| AudioDecoderExt | set_latency     | min       | M |     | Checked in C |
|                 |                 | max       | O |     |         |
| AudioDecoderExt | set_min_latency | num       | M |     | Not checked in C | x
| AudioDecoderExt | set_tolerance   | tolerance | M |     | Not checked in C | x

> @slomo: Latency see above, tolerance mandatory

| scope           | function      | arg.      | M | ret | comment |
|:--------------- |:------------- |:--------- |:-:|:---:| ------- |
| AudioEncoderExt | tolerance     |           |   |  M  | Might not be set? |
| AudioEncoderExt | set_latency   | min       | M |     | Checked in C |
|                 |               | max       | O |     |         |
| AudioEncoderExt | set_tolerance | tolerance | M |     | Not checked in C | x

> @slomo: Same as for decoder

| scope            | function                | arg.                | M | ret | comment |
|:---------------- |:----------------------- |:------------------- |:-:|:---:| ------- |
| AudioStreamAlign | new                     | alignment_threshold | M |     | Not checked in C | x
|                  |                         | discont_wait        | M |     | Not checked in C | x
| AudioStreamAlign | alignment_threshold     |                     |   |  M  |         |
| AudioStreamAlign | discont_wait            |                     |   |  M  |         |
| AudioStreamAlign | timestamp_at_discont    |                     |   |  O  |         |
| AudioStreamAlign | set_alignment_threshold | alignment_threshold | M |     | Not checked in C | x
| AudioStreamAlign | set_discont_wait        | discont_wait        | M |     | Not checked in C | x

> @slomo: All mandatory except for `timestamp_at_discont` (optional), needs C checks.

## gstreamer-base

### Manual

| scope         | function           | arg. | M | ret | comment |
|:------------- |:------------------ |:---- |:-:|:---:| ------- |
| UniqueAdapter | dts_at_discont     |      |   |  O  | Doc: ... or None |
| UniqueAdapter | prev_dts           |      |   |  O  | Doc: reset to None when cleared |
| UniqueAdapter | prev_dts_at_offset |      |   |  O  | Doc: reset to None when cleared |
| UniqueAdapter | prev_pts           |      |   |  O  | Doc: reset to None when cleared |
| UniqueAdapter | prev_pts_at_offset |      |   |  O  | Doc: reset to None when cleared |
| UniqueAdapter | pts_at_discont     |      |   |  O  | Doc: ... or None |

> @slomo: ✅

| scope               | function                 | arg.                 | M | ret | comment |
|:------------------- |:------------------------ |:-------------------- |:-:|:---:| ------- |
| AggregatorExtManual | min_upstream_latency     |                      |   |  M  | Defaults to 0 |
| AggregatorExtManual | set_min_upstream_latency | min_upstream_latency | M |     | Doc: only taken into account when > than reported, but no validity check AFAICT | Couldn't update C: set_property
| AggregatorExtManual | selected_samples         | pts                  | O |     | No check in C |
|                     |                          | dts                  | O |     | No check in C |
|                     |                          | duration             | O |     | No check in C |

> @slomo: ✅ (min upstream latency needs C checks)

### Auto

| scope   | function           | arg. | M | ret | comment |
|:------- |:------------------ |:---- |:-:|:---:| ------- |
| Adapter | dts_at_discont     |      |   |  O  | Doc: ... or None |
| Adapter | prev_dts           |      |   |  O  | Doc: reset to None when cleared |
| Adapter | prev_dts_at_offset |      |   |  O  | Doc: reset to None when cleared |
| Adapter | prev_pts           |      |   |  O  | Doc: reset to None when cleared |
| Adapter | prev_pts_at_offset |      |   |  O  | Doc: reset to None when cleared |
| Adapter | pts_at_discont     |      |   |  O  | Doc: ... or None |

> @slomo: ✅

| scope         | function             | arg.        | M | ret | comment |
|:------------- |:-------------------- |:----------- |:-:|:---:| ------- |
| AggregatorExt | latency              |             |   |  O  | Doc: ... or None if elem does not sync |
| AggregatorExt | set_latency          | min_latency | M |     | Checked in C |
|               |                      | max_latency | O |     | Not checked in C |
| AggregatorExt | simple_get_next_time |             |   |  O  | Probably not always known |

> @slomo: min mandatory, max optional

| scope        | function        | arg.        | M | ret | comment |
|:------------ |:--------------- |:----------- |:-:|:---:| ------- |
| BaseParseExt | add_index_entry | ts          | M |     | No check in C code | x
| BaseParseExt | set_latency     | min_latency | M |     | Checked in C |
|              |                 | max_latency | O |     | Not checked in C | fixed to use IS_VALID macro

> @slomo: ✅ but needs C checks

| scope       | function                | arg.                | M | ret | comment |
|:----------- |:----------------------- |:------------------- |:-:|:---:| ------- |
| BaseSinkExt | latency                 |                     |   |  M  |  |
| BaseSinkExt | processing_deadline     |                     |   |  M  | Code: init @ 20ms, but range check before adding to min latency |
| BaseSinkExt | render_delay            |                     |   |  M  | Code: init @ 0, no check when added to min latency |
| BaseSinkExt | set_processing_deadline | processing_deadline |   |  M  | range check before adding to min latency |
| BaseSinkExt | set_render_delay        | delay               | M |     | No check in C  | x
| BaseSinkExt | wait                    | time                | O |     | None => don't timeout? But I don't see anything like this in the code |
| BaseSinkExt | wait_clock              | time                | M |     | Doc: None => return GstClock::BadTime |

> @slomo: latency, processing latency, delay all mandatory, needs C checks. wait is optional, wait clock is mandatory

| scope            | function   | arg.      | M | ret | comment |
|:---------------- |:---------- |:--------- |:-:|:---:| ------- |
| BaseTransformExt | update_qos | timestamp | M |     | No check in C + used to compute ealiest_time (with possible overflow) | x

> @slomo: ✅ but needs C checks

### Subclass

| scope             | function         | arg. | M | ret | comment |
|:----------------- |:---------------- |:---- |:-:|:---:| ------- |
| AggregatorImpl    | next_time        |      |   |  O  | Not always known |
| AggregatorImplExt | parent_next_time |      |   |  O  | Same as ^ |

> @slomo: ✅

| scope          | function     | arg. | M | ret | comment |
|:-------------- |:------------ |:---- |:-:|:---:| ------- |
| BaseSrcImpl    | times        |      |   |  O  | Probably not always known |
| BaseSrcImplExt | parent_times |      |   |  O  | Same as ^ |

> @slomo: ✅

## gstreamer-check

### Manual

| scope   | function              | arg.    | M | ret | comment |
|:------- |:--------------------- |:------- |:-:|:---:| ------- |
| Harness | last_pushed_timestamp |         |   |  O  | Doc: None when no Buffer has been pushed |
| Harness | query_latency         |         |   |  O  | Code: init @ None |
| Harness | set_time              | time    | M |     | Code: assert in gst_test_clock_set_time |
| Harness | set_upstream_latency  | latency | M |     |  | x

> @slomo: correct but upstream latency is mandatory

### Auto

| scope     | function        | arg.       | M | ret | comment |
|:--------- |:--------------- |:---------- |:-:|:---:| ------- |
| TestClock | with_start_time | start_time | M |     | Code: assert |
| TestClock | next_entry_time |            |   |  M  | Check in C |
| TestClock | set_time        | new_time   | M |     | Code: assert |

> @slomo: ✅

## gstreamer-controller

### Manual

| scope        | function  | arg. | M | ret | comment |
|:------------ |:--------- |:---- |:-:|:---:| ------- |
| ControlPoint | timestamp |      |   |  M  | Probably not always known |

> @slomo: mandatory

### Auto

| scope                      | function | arg.      | M | ret | comment |
|:-------------------------- |:-------- |:--------- |:-:|:---:| ------- |
| TimedValueControlSourceExt | set      | timestamp | M |     | Checked in C |
| TimedValueControlSourceExt | unset    | timestamp | M |     | Checked in C |

> @slomo: ✅

## gstreamer-editing-services

### Auto

| scope   | function                         | arg.          | M | ret | comment |
|:------- |:-------------------------------- |:------------- |:-:|:---:| ------- |
| ClipExt | duration_limit                   |               |   |  M  | Code: computation seems to bound check duration_limit |
| ClipExt | internal_time_from_timeline_time | timeline_time | O |  O  | Code: arg checked on without None being an error and there are other unrelated checks beforehands |
| ClipExt | timeline_time_from_internal_time | internal_time | O |  O  | Code: arg checked on without None being an error and there are other unrelated checks beforehands, Doc: ret None when conversion couldn't be performed |

> @slomo: ✅ (but I don't know this API well)

| scope    | function          | arg.     | M | ret | comment |
|:-------- |:----------------- |:-------- |:-:|:---:| ------- |
| LayerExt | add_asset         | start    | O |     | See below |
|          |                   | inpoint  | O |     | See below |
|          |                   | duration | O |     | See below |
| LayerExt | add_asset_full    | start    | O |     | Doc: None => add to the end |
|          |                   | inpoint  | O |     | Not checked in C |
|          |                   | duration | O |     | Code: skip if None |
| LayerExt | clips_in_interval | start    | O |     | Unbounded if None? Computation seems to allow this |
|          |                   | end      | O |     | Unbounded if None? Computation seems to allow this |
| LayerExt | duration          |          |   |  M  | Code: I can't find a reason why it could end up being None |

> @slomo: ✅ (but I don't know this API well)

| scope       | function              | arg.              | M | ret | comment |
|:----------- |:--------------------- |:----------------- |:-:|:---:| ------- |
| TimelineExt | duration              |                   |   |  M  | Should be 0 when unknown and init @ 0, but property accepts G_MAXUINT64 |
| TimelineExt | frame_at              | timestamp         | M |     | Checked in C |
| TimelineExt | frame_time            |                   |   |  O  | Code: ret. None if frame_number is invalid |
| TimelineExt | snapping_distance     |                   |   |  O  | Init @ 0, but set_snapping_distance doesn't check input |
| TimelineExt | paste_element         | position          | M |     | Not checked in C, but I don't see what it could do with None | x
| TimelineExt | set_snapping_distance | snapping_distance | O |     | Not checked in C |

> @slomo: ✅ (but I don't know this API well)

| scope              | function         | arg.           | M | ret | comment |
|:------------------ |:---------------- |:-------------- |:-:|:---:| ------- |
| TimelineElementExt | duration         |                |   |  M  | See above |
| TimelineElementExt | inpoint          |                |   |  M  | Not sure if can be None |
| TimelineElementExt | max_duration     |                |   |  O  | Not sure if can be None |
| TimelineElementExt | start            |                |   |  O  | Might not be known in some states? |
| TimelineElementExt | paste            | paste_position | M |     | Not checked in C, but I don't see what it could do with None | x
| TimelineElementExt | ripple           | start          | M |     | What would be the point in passing None? | x
| TimelineElementExt | ripple_end       | end            | M |     | What would be the point in passing None? | x
| TimelineElementExt | roll_end         | end            | M |     | What would be the point in passing None? | x
| TimelineElementExt | roll_start       | start          | M |     | What would be the point in passing None? | x
| TimelineElementExt | set_duration     | duration       | O |     | None to unset? |
| TimelineElementExt | set_inpoint      | inpoint        | M |     | Returns false if inpoint > maxduration, so no use passing None |
| TimelineElementExt | set_max_duration | maxduration    | O |     | Unbounded when None? Yes |
| TimelineElementExt | set_start        | start          | M |     | Not checked in C |
| TimelineElementExt | trim             | start          | M |     | What would be the point in passing None? |

> @slomo: ✅ (but I don't know this API well)

| scope           | function     | arg. | M | ret | comment |
|:--------------- |:------------ |:---- |:-:|:---:| ------- |
| UriClipAssetExt | duration     |      |   |  O  | Could it be unknown? |
| UriClipAssetExt | max_duration |      |   |  O  | Could be unbounded? |

> @slomo: ✅ (but I don't know this API well)

## gstreamer-net

### Manual

| scope          | function | arg.      | M | ret | comment |
|:-------------- |:-------- |:--------- |:-:|:---:| ------- |
| NetClientClock | new      | base_time | M |     | Init @ 0 + checked in C |

> @slomo: ✅

| scope    | function | arg.      | M | ret | comment |
|:-------- |:-------- |:--------- |:-:|:---:| ------- |
| NtpClock | new      | base_time | M |     | Save as above I guess |

> @slomo: ✅

## gstreamer-pbutils

### Manual

| scope      | function    | arg.    | M | ret | comment |
|:---------- |:----------- |:------- |:-:|:---:| ------- |
| Discoverer | set_timeout | timeout | M |     | See below | x
| Discoverer | timeout     |         |   |  M  | See below |

> @slomo: ✅

### Auto

| scope      | function | arg.    | M | ret | comment |
|:---------- |:-------- |:------- |:-:|:---:| ------- |
| Discoverer | new      | timeout | M |     | Doc: allowed value 1s <= timeout <= 1h, but not checked | x

> @slomo: ✅

| scope          | function | arg. | M | ret | comment |
|:-------------- |:-------- |:---- |:-:|:---:| ------- |
| DiscovererInfo | duration |      |   |  O  | Might not be knonw? |

> @slomo: ✅

## gstreamer-player

### Manual

### Auto

| scope  | function | arg.     | M | ret | comment |
|:------ |:-------- |:-------- |:-:|:---:| ------- |
| Player | duration |          |   |  O  | Might not be knonw? |
| Player | position |          |   |  O  | Might not be knonw in some states? |
| Player | seek     | position | M |     | Checked in C |

> @slomo: ✅

| scope           | function | arg. | M | ret | comment |
|:--------------- |:-------- |:---- |:-:|:---:| ------- |
| PlayerMediaInfo | duration |      |   |  O  | Might not be knonw? |

> @slomo: ✅

## gstreamer-rtsp-server

### Auto

| scope        | function                | arg. | M | ret | comment |
|:------------ |:----------------------- |:---- |:-:|:---:| ------- |
| RTSPMediaExt | base_time               |      |   |  O  | Be consistent with decision taken for gst::ElementExt |
| RTSPMediaExt | retransmission_time     |      |   |  O  |         |
| RTSPMediaExt | set_retransmission_time | time |   |  O  |         |

> @slomo: ✅

| scope               | function                | arg. | M | ret | comment |
|:------------------- |:----------------------- |:---- |:-:|:---:| ------- |
| RTSPMediaFactoryExt | retransmission_time     |      |   |  O  | See below |
| RTSPMediaFactoryExt | set_retransmission_time | time | O |     | Not checked in C, unbounded when None? |

> @slomo: ✅

| scope               | function  | arg. | M | ret | comment |
|:------------------- |:--------- |:---- |:-:|:---:| ------- |
| RTSPSessionMediaExt | base_time |      |   |  O  | Be consistent with decision taken for gst::ElementExt |

> @slomo: ✅

| scope         | function                | arg. | M | ret | comment |
|:------------- |:----------------------- |:---- |:-:|:---:| ------- |
| RTSPStreamExt | retransmission_time     |      |   |  O  | See above, I guess |
| RTSPStreamExt | rtpinfo                 |      |   |  T  | Ret. (.., running_time: O) Code: returns None in some cndts |
| RTSPStreamExt | set_retransmission_time | time |   |  O  | Not checked in C, same as above I guess |

> @slomo: ✅ and rtpinfo optional

| scope                  | function | arg.       | M | ret | comment |
|:---------------------- |:-------- |:---------- |:-:|:---:| ------- |
| RTSPStreamTransportExt | rtpinfo  | start_time | O |     | Code: scaling not performed if None |

> @slomo: ✅

### Subclass

| scope            | function              | arg. | M | ret | comment |
|:---------------- |:--------------------- |:---- |:-:|:---:| ------- |
| RTSPMediaImpl    | query_position        |      |   |  O  | Transitively returns None in some cndts |
| RTSPMediaImpl    | query_stop            |      |   |  O  | Transitively returns None in some cndts |
| RTSPMediaImplExt | parent_query_position |      |   |  O  | Same as above |
| RTSPMediaImplExt | parent_query_stop     |      |   |  O  | Same as above |

> @slomo: ✅

## gstreamer-video

### Manual

| scope           | function     | arg.     | M | ret | comment |
|:--------------- |:------------ |:-------- |:-:|:---:| ------- |
| VideoCodecFrame | dts          |          |   |  O  | Might not be known? |
| VideoCodecFrame | set_dts      | dts      | O |     | Might be need to unset? |
| VideoCodecFrame | pts          |          |   |  O  | Might not be known? |
| VideoCodecFrame | set_pts      | pts      | O |     | Might be need to unset? |
| VideoCodecFrame | duration     |          |   |  O  | Might be unknown in some states? |
| VideoCodecFrame | set_duration | duration | O |     | Might be need to unset? |
| VideoCodecFrame | deadline     |          |   |  O  | Might not be known? |

> @slomo: ✅

| scope                 | function    | arg.        | M | ret | comment |
|:--------------------- |:----------- |:----------- |:-:|:---:| ------- |
| VideoDecoderExtManual | latency     |             |   |  T  | Ret: (min: M, max: O) |
| VideoDecoderExtManual | set_latency | min_latency | M |     | Checked in C |
|                       |             | max_latency | O |     | None => Unbounded? |

> @slomo: ✅

| scope                 | function    | arg.        | M | ret | comment |
|:--------------------- |:----------- |:----------- |:-:|:---:| ------- |
| VideoEncoderExtManual | latency     |             |   |  T  | Ret: (min: M, max: O) |
| VideoEncoderExtManual | set_latency | min_latency | M |     | Check in C |
|                       |             | max_latency | O |     | None => Unbounded? |

> @slomo: ✅

| scope                              | function     | arg.         | M | ret | comment |
|:---------------------------------- |:------------ |:------------ |:-:|:---:| ------- |
| DownstreamForceKeyUnitEventBuilder | timestamp    | timestamp    | O |     | Can call with a var. `Option<ClockTime>`? |
| DownstreamForceKeyUnitEventBuilder | stream_time  | stream_time  | O |     | Can call with a var. `Option<ClockTime>`? |
| DownstreamForceKeyUnitEventBuilder | running_time | running_time | O |     | Can call with a var. `Option<ClockTime>`? |
| UpstreamForceKeyUnitEventBuilder   | running_time | running_time | O |     | Can call with a var. `Option<ClockTime>`? |

> @slomo: ✅

### Auto

| scope           | function                        | arg.     | M | ret | comment |
|:--------------- |:------------------------------- |:-------- |:-:|:---:| ------- |
| VideoEncoderExt | min_force_key_unit_interval     |          |   |  O  | See below |
| VideoEncoderExt | set_min_force_key_unit_interval | interval | O |     | Doc: None => ignore force-keyunit |
| VideoEncoderExt | set_min_pts                     | min_pts  | O |     | None => unbounded? |

> @slomo: ✅

